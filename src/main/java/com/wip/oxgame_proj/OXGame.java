/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.oxgame_proj;

import java.util.Scanner;

/**
 *
 * @author WIP
 */
public class OXGame {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        String o = "none", x = "none", draw = "none";
        String turnO = "yes", turnX = "no";
        int row = 0, col = 0, count = 0;

        String[][] game = new String[3][3];

        //Welcome to Game
        System.out.println("Welcome to OX Game");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                game[i][j] = "-";
                System.out.print(game[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();

        //Process Game
        while (!o.equals("win") || !x.equals("win") || !draw.equals("win")) {

            //Turn O
            if (turnO.equals("yes")) {
                System.out.println("Turn O");
                System.out.println("Please input row, col : ");
                row = kb.nextInt();
                col = kb.nextInt();
                row -= 1;
                col -= 1;

                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        if (i == row && j == col) {
                            game[i][j] = "O";
                            break;
                        }
                    }
                }
                turnO = "no";
                turnX = "yes";
            }//Turn X
            else if (turnX.equals("yes")) {
                System.out.println("Turn X");
                System.out.println("Please input row, col : ");
                row = kb.nextInt();
                col = kb.nextInt();
                row -= 1;
                col -= 1;
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        if ((i == row && j == col)) {
                            game[i][j] = "X";
                            break;
                        }
                    }
                }

                turnX = "no";
                turnO = "yes";
            }

            //print Game update
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    System.out.print(game[i][j] + " ");
                }
                System.out.println();
            }

            //check result 
            //O
            if ((game[0][0].equals("O") && game[1][0].equals("O") && game[2][0].equals("O"))
                    || (game[0][1].equals("O") && game[1][1].equals("O") && game[2][1].equals("O"))
                    || (game[0][2].equals("O") && game[1][2].equals("O") && game[2][2].equals("O"))
                    || (game[0][0].equals("O") && game[0][1].equals("O") && game[0][2].equals("O"))
                    || (game[1][0].equals("O") && game[1][1].equals("O") && game[1][2].equals("O"))
                    || (game[1][0].equals("O") && game[1][1].equals("O") && game[1][2].equals("O"))
                    || (game[0][0].equals("O") && game[1][1].equals("O") && game[2][2].equals("O"))
                    || (game[0][2].equals("O") && game[1][1].equals("O") && game[2][0].equals("O"))) {
                o = "win";
                break;
            } //X
            else if ((game[0][0].equals("X") && game[0][1].equals("X") && game[0][2].equals("X"))
                    || (game[0][0].equals("X") && game[1][0].equals("X") && game[2][0].equals("X"))) {
                x = "win";
                break;
            } //draw
            else if (count == 9 && o.equals("none") && x.equals("none")) {
                draw = "win";
                break;
            }

            count++;
            System.out.println();
        }

        //Result
        if (o.equals("win")) {
            System.out.println(">>> O Win <<<");
        } else if (x.equals("win")) {
            System.out.println(">>> X Win <<<");
        } else {
            System.out.println(">>> DRAW <<<");
        }
    }
}
